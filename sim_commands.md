#Sim Commands
Applies to sim900 module. These commands are pretty standard. They are logged here for reference.

#Initialising
To gather proper details on incoming calls, and provide better parsing stability, enable ECHO, CLIP and CRC settings on bootup.

	AT+E1+CLIP=1;+CRC=1

E1 			enables errors
+CLIP=1 	Calling Line Identification Presentation
+CRC=1		Cellular Result Codes

This will enable the proper unsolicited responses when somebody is calling, for example an incoming call will look like this:

	+CRING: VOICE

	+CLIP: "+3165nnnnnnnnn",145,"",,"",0

Note the lack of an OK termination.

#Current Status
Retrieve the signal quality (includes response):

	AT+CSQ

	+CSQ: <x>,<y>

<x> is the signal strength 0-17.

Retrieve the current network provider:

	AT+COPS?

	+COPS: <x>,<y>,"<provider>"

Note the quotes around the provider name.

#SMS
When an SMS is received (while powered up of course), the following unsollicited response is sent:

	AT+CMTI: "SM", <n>

This means that an SMS was save to position [n].

The SMS is stored in some native format. To read the message in clear text we need to set the read mode. This sets SMS read mode to ASCII/Text:

	AT+CMF=1

Read SMS number 3.

	AT+CMGR=3

Sample response:

	+CMGR: "REC UNREAD","+316nnnnnn","","13/12/13,19:33:13+04"
	Magic simplicate!	OK

List all SMS's. Warning the 'ALL' argument is dependent on the ASCII mode (+CMF)

	AT+CMGL="ALL"

Response:

	+CMGL: 1,"REC READ","+316nnnnnnnn","","13/12/04,10:58:11+04"
	Ontvangen

	+CMGL: 2,"REC READ","+31nnnnnnnn","","13/12/05,16:10:19+04"
	1 gemiste oproep(en) van +316nnnnnnn
	Laatste oproep: 16:10 uur 5/12/13.
	Er is geen bericht ingesproken. Dit is een gratis bericht van VoiceMail.

	+CMGL: 3,"REC READ","+316nnnnnnnn","","13/12/13,19:33:13+04"
	Magic simplicate!

	+CMGL: 4,"REC READ","+316nnnnnnnn","","13/12/13,19:35:32+04"
	Nogmaals test

	OK

#RTCSome phones and providers may even broadcast RTC information, but it would need a GPRS call to get it working.Get RTC:
	AT+CCLK?
Response:
	+CCLK: "13/12/05,19:55:05+04"
	OK
Set RTC (mind the quotes)
	AT+CCLK="13/12/05,19:55:00+04"