#include "ST7565r.h"

ST7565r::ST7565r(uint8_t data, uint8_t clock, uint8_t a0, uint8_t rst, uint8_t cs)
{
  pin_data = data;
  pin_clock = clock;
  pin_a0 = a0;
  pin_rst = rst;
  pin_cs = cs;

  pinMode(pin_data, OUTPUT);
  pinMode(pin_clock, OUTPUT);
  pinMode(pin_a0, OUTPUT);
  pinMode(pin_rst, OUTPUT);
  if(pin_cs > 0)
  {
    pinMode(pin_cs, OUTPUT);
  }
}

void ST7565r::write(bool data, unsigned char value)
{
  if(pin_cs > 0)
  {
    digitalWrite(pin_cs, 0);
  }
  digitalWrite(pin_a0, data);

  shiftOut(pin_data, pin_clock, MSBFIRST, value);

  if(pin_cs > 0)
  {
    digitalWrite(pin_cs, 1);  
  }
}

void ST7565r::setup(uint8_t contrast)
{
  digitalWrite(pin_rst, false);
  delay(100);
  digitalWrite(pin_rst, true);
  delay(50);

  write(0, CMD_SET_LCD_BIAS_19);    //LCD bias
  delay(1);
  write(0, 0xA0 | 0x1);    //horizontally 'reverse' (ADC select)
  delay(1);
  write(0, 0xC0);    //COM normal
  delay(1);
  write(0, 0x40);    //go back to the top of the display.
  delay(1);
  write(0, CMD_SET_DISPLAY_NORMAL);
  delay(1);
  write(0, CMD_SET_SET_POWER | 0x7);  
  //delay(1);
  //write(0, CMD_SET_RESISTOR_RATIO | 0x1); 
  delay(50);

  clear();

  //turn display on
  write(0, CMD_SET_DISPLAY_ON);

  //set all PTS normal
  write(0, CMD_SET_ALL_POINTS_NORMAL);    
  setContrast(contrast);
}

void ST7565r::setContrast(unsigned char contrast)
{
  write(0, CMD_SET_CONTRAST_MODE);  
  write(0, CMD_SET_CONTRAST_VALUE | (contrast & 0x3F));  
}

void ST7565r::clear()
{
  //clear the buffer
  for(int i = 0; i < 512; i++)
  {
    screenBuffer[i] = 0x00;
  }

  bufferChanged = true;
}

/* copy the buffer to the display 
 * TODO: use update regions for faster refresh and save CPU cycles.
 */
void ST7565r::update()
{
  if(!bufferChanged)
  {
    return;
  }

  //unsigned char *p = screenBuffer;

  for(int row = 0; row < 4; row++)
  {
    write(0, CMD_SET_PAGE | row);  //set page 0
    write(0, CMD_SET_COLUMN_MSB | 0);  //set col 0
    write(0, CMD_SET_COLUMN_LSB | 4);

    for(int i = 0; i < SCREEN_WIDTH; i++)
    { 
      write(1, screenBuffer[(row * SCREEN_WIDTH) + i]);  
    }
  }

  bufferChanged = false;
}

int ST7565r::print(char *text, uint8_t x, uint8_t y, const unsigned char *font, bool reverse)
{
  int width = 0;
  //no word wrapping here.
  while(*text != '\0')
  {
    width += print(*text, x + width, y, font, reverse);
    width += 1; 
    text++;
  }
  return width;
}

int ST7565r::print(char *text, uint8_t y, const unsigned char *font, bool reverse, HorizontalAlignment align)
{
  int width = measure(text, font);
  int x = 0;
  switch(align)
  {
    case ALIGN_LEFT:
      x = 0;
      break;
    case ALIGN_RIGHT:
      x = SCREEN_WIDTH - width;
      break;
    case ALIGN_CENTER:
      x = (SCREEN_WIDTH - width) / 2;
      break;
  }
  print(text, x, y, font, reverse);
}

int ST7565r::measure(char *text, const unsigned char *font)
{
  fontHeader header;
  memcpy_P(&header, font, sizeof(fontHeader));

  if(header.type != FONT_TYPE_PROPORTIONAL)
  {
    return 0;
  }

  if(header.orientation != FONT_ORIENTATION_VERTICAL_CEILING)
  {
    return 0;
  }

  int width = 0;
  while(*text != '\0')
  {
    char c = *text;
    if(c < header.startCharacter || c > header.startCharacter + header.numCharacters)
    {
      return 0;
    }

    int index = c - header.startCharacter;
    int pos;
    pos = pgm_read_byte(&font[5 + index*2]);
    pos <<= 8;
    pos |= pgm_read_byte(&font[6 + index*2]);

    width += pgm_read_byte(&font[pos]);  
    width += 1; 
    text++;
  }
  return width-1;
}

void ST7565r::putPixel(uint8_t value, uint8_t x, uint8_t y)
{
  if(x < 0 || y < 0 || x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT)
  {
   return;
  }
 
  int bufOffset = x + ((y / 8) * SCREEN_WIDTH);
  int bitNum = y & 0x7; 
  int bitValue = 1 << bitNum;
  if(value != 0)
  {
    screenBuffer[bufOffset] |= bitValue;
  }
  else
  {
    screenBuffer[bufOffset] &= ~bitValue;
  }
  
   bufferChanged = true;
}

void ST7565r::fillRectangle(uint8_t value, uint8_t x1, uint8_t y1, uint8_t width, uint8_t height)
{
   for(uint8_t x = x1; x < x1+width-1; x++)
   {
     for(uint8_t y = y1; y < y1+height-1; y++)
     {
       putPixel(value, x, y);
     }
   }
}

int ST7565r::print(char c, uint8_t x, uint8_t y, const unsigned char *font, bool reverse)
{
  //one character at a time, return the width of the printer character, no wordwrap, no optimisations
  fontHeader header;
  memcpy_P(&header, font, sizeof(fontHeader));

  if(header.type != FONT_TYPE_PROPORTIONAL)
  {
    return 0;
  }

  if(header.orientation != FONT_ORIENTATION_VERTICAL_CEILING)
  {
    return 0;
  }

  if(c < header.startCharacter || c > header.startCharacter + header.numCharacters)
  {
    return 0;
  }

  int index = c - header.startCharacter;
  int indexOffset = sizeof(fontHeader);

  int pos;
  pos = pgm_read_byte(&font[5 + index*2]);
  pos <<= 8;
  pos |= pgm_read_byte(&font[6 + index*2]);

  //bytes are available at font[offset];
  int width = pgm_read_byte(&font[pos]);
  pos++;

  for(int col = 0; col < width; col++)
  {
    unsigned char fontData;
    for(int row = 0; row < header.height; row++)
    {
        if(row % 8 == 0)
        {
          fontData = pgm_read_byte(&font[pos]); 
          pos++;
        }
        if(fontData & (1 << (row % 8)))
        {
          putPixel(1 ^ reverse, x + col, y + row);
        }
    }
  }
  
  return width;  
}






