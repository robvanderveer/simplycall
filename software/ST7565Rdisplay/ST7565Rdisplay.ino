
/**** WARNING: DO NOT UPLOAD THIS DEMO CODE TO A PROJECT, IT MIGHT BURN THE CPU */

#include "ST7565r.h"
#include "font_custom.h"
#include "menu.h"

uint8_t const PIN_INUSE = 19;

uint8_t const LCD_PIN_CS = 18;  
uint8_t const LCD_PIN_RESET = 17;
uint8_t const LCD_PIN_A0 = 16;
uint8_t const LCD_PIN_CLOCK = 15;
uint8_t const LCD_PIN_SI = 14;    //PIN_SI

const uint8_t DATA = 2;    //PD2
const uint8_t CLOCK = 4;    //PD4
const uint8_t LATCH = 3;    //PD3

ST7565r lcd(LCD_PIN_SI, LCD_PIN_CLOCK, LCD_PIN_A0, LCD_PIN_RESET, LCD_PIN_CS);

menuItemType mainMenu[] = {
 { 1, '1', "Missed Calls", NULL, NULL },
 { 2, '2', "Messages", NULL,NULL },
 { 3, '3', "Phonebook", NULL, NULL },
 { 9, '9', "Settings", NULL, NULL }
};

bool onOff;

void setup()
{
  pinMode(LATCH, OUTPUT);
  pinMode(DATA, OUTPUT);  
  pinMode(CLOCK, OUTPUT);
  pinMode(PIN_INUSE, OUTPUT);
  
  digitalWrite(PIN_INUSE, HIGH); //blue led on.
  sendShifter(0x01);    //backlight on
  
  Serial.begin(57600);
  Serial.println("boot");
  //digitalWrite(13, true);
  
  lcd.setup(0x00);
  lcd.update();

  // reverse mode test 
  //drawStatusBar("NL KPN", "H", 0);
   
  // large font test
  char *str = "no missed calls";
  lcd.print(str, 13, Tahoma14, 0, ALIGN_CENTER);
  
  lcd.update();
}

int signalLevel = 0;
int menuItem = 0;

void loop()
{
  //onOff = !onOff;
  //sendShifter(onOff?0x01:0);
  
  signalLevel++;
  if(signalLevel > 4)
  {
    signalLevel = 0;
  }
    // reverse mode test 
  //drawStatusBar("NL KPN", "KL", signalLevel);
  
  //menu sample.
  char *menu[] = { "1 Call History", "2 Read Message", "3 Write Message", "4 Phonebook", "5 Games", "9 Settings" };
  drawMenu(mainMenu, 4, menuItem);
  
  lcd.update();
  
  delay(500);
  //Serial.println(menuItem);
 
  
  menuItem++;
  if(menuItem > 3)
  {
    menuItem = 0; 
  }
}


void sendShifter(uint8_t value)
{
  digitalWrite(LATCH, LOW);
  shiftOut(DATA, CLOCK, MSBFIRST, value);
     //delay(50);
  digitalWrite(LATCH, HIGH);
}
