#include <arduino.h>

#ifndef MENU_H
#define MENU_H

typedef bool (*menuCallBack)(uint8_t id);

typedef struct menuItem
{
  uint8_t menuId;
  char shortcutKey;
  char *title;
  menuCallBack onIsEnabled; 
  menuCallBack onSelected;
} menuItemType;

#endif
