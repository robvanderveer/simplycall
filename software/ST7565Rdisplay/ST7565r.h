#include "arduino.h"

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 32

uint8_t const CMD_SET_PAGE = 0b10110000;
uint8_t const CMD_SET_COLUMN_MSB = 0b00010000;
uint8_t const CMD_SET_COLUMN_LSB = 0b00000000;
uint8_t const CMD_SET_SET_POWER = 0x28;
uint8_t const CMD_SET_LCD_BIAS_19 = 0xA2;
uint8_t const CMD_SET_LCD_BIAS_17 = 0xA3;

uint8_t const CMD_SET_DISPLAY_NORMAL = 0xA6;
uint8_t const CMD_SET_DISPLAY_REVERSE = 0xA7;

uint8_t const CMD_SET_DISPLAY_ON = 0xAF;
uint8_t const CMD_SET_DISPLAY_OFF = 0xAE;

uint8_t const CMD_SET_RESISTOR_RATIO = 0x20;
uint8_t const CMD_SET_ALL_POINTS_NORMAL = 0xA4;
uint8_t const CMD_SET_ALL_POINTS_ON = 0xA5;

uint8_t const CMD_SET_CONTRAST_MODE = 0x81;
uint8_t const CMD_SET_CONTRAST_VALUE = 0x00;

typedef enum 
{
  ALIGN_LEFT = 0,
  ALIGN_RIGHT = 1,
  ALIGN_CENTER = 2
} HorizontalAlignment;

class ST7565r
{
  private:
    uint8_t pin_cs;
    uint8_t pin_a0;
    uint8_t pin_data;
    uint8_t pin_clock;
    uint8_t pin_rst;
    unsigned char screenBuffer[SCREEN_HEIGHT * SCREEN_WIDTH / 8];
    void write(bool data, unsigned char value);
    bool bufferChanged;
    
  public:
    ST7565r(uint8_t data, uint8_t clock, uint8_t a0, uint8_t rst, uint8_t cs);
    void setup(uint8_t contrast);
    void clear();
    void update();
    void setContrast(uint8_t contrast); 
    void putPixel(uint8_t value, uint8_t x, uint8_t y);
    void fillRectangle(uint8_t value, uint8_t x1, uint8_t y1, uint8_t width, uint8_t height);
    // when printing reverse, please make sure you have filled a rectange below.
    int print(char *text, uint8_t x, uint8_t y, const unsigned char *font, bool reverse);
    int print(char c, uint8_t x, uint8_t y, const unsigned char *font, bool reverse);
    int print(char *text, uint8_t y, const unsigned char *font, bool reverse, HorizontalAlignment align);
    int measure(char *text, const unsigned char *font);
};

#define FONT_TYPE_MONOSPACE     0x00
#define FONT_TYPE_PROPORTIONAL  0x01

#define FONT_ORIENTATION_VERTICAL_CEILING  0x02



typedef struct 
{
   uint8_t type;
   uint8_t orientation;
   uint8_t startCharacter;
   uint8_t numCharacters;
   uint8_t height;
} fontHeader;
