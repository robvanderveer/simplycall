

// title goes on the left, icon set goes on the right.
// signal level goes from 0-4.
void drawStatusBar(char *title, char *icons, int signalLevel)
{
  lcd.fillRectangle(0, 0, 0, SCREEN_WIDTH, 12);
  switch(signalLevel)
  {
  case 0:
    lcd.print("A", 0, 0, glyphs, 0);
    break;
  case 1:
    lcd.print("AB", 0, 0, glyphs, 0);
    break;
  case 2:
    lcd.print("ABC", 0, 0, glyphs, 0);
    break;
  case 3:
    lcd.print("ABCD", 0, 0, glyphs, 0);
    break;
  case 4:
    lcd.print("ABCDE", 0, 0, glyphs, 0);
    break;
  }

  lcd.print(title, 0, Verdana8, 0, ALIGN_CENTER);
  lcd.print(icons, 0, glyphs, 0, ALIGN_RIGHT);
}




// draw a menu. the selection will center around the 'activeIndex'
void drawMenu(menuItemType *menus, int numItems, int activeIndex)
{
  if(activeIndex < 0)
  {
    activeIndex = 0;
  }
  if(activeIndex > numItems - 1)
  {
    activeIndex = numItems - 1;
  }

  //draw max 4 rows.
  //clear the screen.
  lcd.clear();

  int item = 0;  //top item to display.
  if(numItems > 3)
  {
    item = activeIndex - 1;
    if(item == numItems - 2)
    {
      item = numItems - 3;
    }
    if(item < 0)
    {
      item = 0;
    }
  }

  if(item > 0)
  {
    lcd.print("O", SCREEN_WIDTH - 30, 0, glyphs, 0);
  }
  if(item < numItems - 3)
  {
    lcd.print("P", SCREEN_WIDTH - 30, SCREEN_HEIGHT-9, glyphs, 0);
  }



  int rowHeight = 11;
  for(int row = 0; row < 3 && item < numItems; item++, row++)
  {
    if(item == activeIndex)
    {
      //reverse.
      lcd.fillRectangle(1, 0, row * rowHeight, SCREEN_WIDTH - 30, rowHeight+1);
      lcd.print(menus[item].title, 1, row * rowHeight, Verdana8, 1); 
    }
    else
    {
      lcd.print(menus[item].title, 1, row * rowHeight, Verdana8, 0); 
    } 
  }

  //print 'END'
  lcd.print("H", SCREEN_HEIGHT-9, glyphs, 0, ALIGN_RIGHT);

}




