#ifndef HARDWARE_H
#define HARDWARE_H

#if !defined(__AVR_ATmega328P__)
#error Make sure you select '[USBASP] ATmega328p @16Mhz' from the boards menu.
#else
#if F_CPU != 16000000
#error Make sure you select '[USBASP] ATmega328p @16Mhz' from the boards menu. You got the right board, but not the right speed.
#endif
#endif

// Pin assignments 
uint8_t const PIN_INUSE = 19;
uint8_t const PIN_DEBUG_TX = 13;    //HARDWARE defined by debug.h
uint8_t const PIN_ROAM = 12;

uint8_t const PIN_LCD_CS = 0;    //CS not used  
uint8_t const PIN_LCD_RESET = 18;
uint8_t const PIN_LCD_A0 = 17;
uint8_t const PIN_LCD_CLOCK = 16;
uint8_t const PIN_LCD_SI = 15;    //PIN_SI

const uint8_t PIN_SHIFTER_DATA = 2;    //PD2
const uint8_t PIN_SHIFTER_CLOCK = 4;    //PD4
const uint8_t PIN_SHIFTER_LATCH = 3;    //PD3

const uint8_t PIN_KEYBOARD_C0 = 9;
const uint8_t PIN_KEYBOARD_C1 = 10;
const uint8_t PIN_KEYBOARD_C2 = 11;

// shifter section
const uint8_t SHIFTER_BACKLIGHT = 0x01;
const uint8_t SHIFTER_ROW1 = 0x02;
const uint8_t SHIFTER_ROW2 = 0x04;
const uint8_t SHIFTER_ROW3 = 0x08;
const uint8_t SHIFTER_ROW4 = 0x10;
const uint8_t SHIFTER_ROW5 = 0x20;
const uint8_t SHIFTER_ROW6 = 0x40;
const uint8_t SHIFTER_ROW_MASK = 0x7e;
const uint8_t SHIFTER_NO_SERVICE = 0x80;

// power section
const uint8_t PIN_OUT_PWM_RINGER = 5;
const uint8_t PIN_OUT_GSM_POWER = 6;
const uint8_t PIN_IN_POWERBUTTON = 7;
const uint8_t PIN_OUT_ENABLE_REGULATORS = 8;
const uint8_t PIN_IN_BATTERY_POWER = 14;

const float VBAT_R1 = 480.0;
const float VBAT_R2 = 100.0;

const unsigned int SERIAL_BAUDRATE = 57600;

const unsigned long POWER_TIMEOUT = 1000U;

#endif

