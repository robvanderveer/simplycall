#include <arduino.h>

#ifndef MENU_H
#define MENU_H

typedef struct menuItem;

typedef bool (*menuCallBack)(const menuItem *);

typedef struct menuItem
{
  uint8_t menuId;
  uint8_t parentId;
  const char glyph;
  PGM_P title; 
  menuCallBack onIsEnabled;   //can be implemented to determine if the menu is enabled.
  menuCallBack onSelected;    //can be implemented to implement selection detection like changing settings or activating the hoverID
  uint8_t menuParameter;      //will be passed to the onSelected event.
} menuItemType;

class MenuHandler
{
  private:
    const menuItemType  *_menu;
    uint8_t       _menuCount;
    uint8_t       _currentMenu;
    uint8_t       _hoverItemID;
    
    const menuItemType *getMenuForID(uint8_t id)
    {
      for(uint8_t i = 0; i < _menuCount; i++)
      {
        if(_menu[i].menuId == id)
        {
          return &_menu[i];
        }
      }
      
      return NULL;
    }
  
  public:
    MenuHandler(const menuItemType menu[], uint8_t menuCount)
    {
      _menu = menu;
      _menuCount = menuCount;
      reset();
    }
    
    void reset()
    {
      _currentMenu = 0;
      activateFirstChild();
    }
    
    void selectCurrent()
    {
       // perform selection operation on the active menu (which is hoverItemID)
       const menuItemType *item = getMenuForID(_hoverItemID);
       if(item != NULL)
       {
         if(item->onSelected)
         {
            //perform select. Usually a leaf.
           item->onSelected(item); 
           
         }
         else
         {
           _currentMenu = item->menuId;
           
           activateFirstChild();
         }
       }
    }
    
    void activateFirstChild()
    {
       //activate the first child for hover operation.
       const menuItemType *firstChild = getChild(0);
       if(firstChild)
       {
         _hoverItemID = firstChild->menuId;
       }
    }
    
    uint8_t getNumberOfChildItems(uint8_t *activeIndex)
    {
       uint8_t count = 0;
       *activeIndex = 0;
         
       //returns the number of submenus.
       for(uint8_t i = 0; i < _menuCount; i++)
       {
         if(_menu[i].parentId == _currentMenu)
         {
           if(_menu[i].menuId == _hoverItemID)
           {
             *activeIndex = count;
           }
           
           count++;
         }
       }
       return count;
    }
    const menuItemType *getChild(uint8_t index)
    {
       uint8_t count = 0;
       for(uint8_t i = 0; i < _menuCount; i++)
       {
         if(_menu[i].parentId == _currentMenu)
         {
           if(count == index)
           {
             return &_menu[i];
           }
           count++;
         }
       }
       return NULL;
    }
    
    void selectNext()
    {
      uint8_t activeIndex;
      uint8_t items = getNumberOfChildItems(&activeIndex);
      
      if(activeIndex < items-1)
      {
        activeIndex++;
      }
      else
      {
        activeIndex = 0;
      }
      
      const menuItemType *item = getChild(activeIndex);
      if(item != NULL)
      {
        _hoverItemID = item->menuId;
      }
    }
    //return false when the top menu is reached
    bool selectBack()
    {
      const menuItemType *item = getMenuForID(_currentMenu);
      if(item != NULL)
      {
        _currentMenu = item->parentId;
        
        activateFirstChild();
        return true;
      }
      return false;
    }
    bool selectUsingKey(char c);
};
#endif

