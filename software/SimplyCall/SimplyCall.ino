/*
 * SimplyCall revision 2
 * Rob van der Veer, december 2014
 *
 * New Hardware, new software
 *
 * ATMega328p, 16Mhz crystal.
 */   
 
#include <MemoryFree.h>
#include "ST7565r.h"
#include "Hayes.h"
#include "menu.h"
#include "timedEvent.h"
#include "keyboard.h"
#include "viewController.h"
#include "debug.h"
#include "hardware.h"
#include "player.h"
#include "Time.h"

// key matrix

const char keyboardMatrix[] = 
{
   '1', '2', '3',
   '4', '5', '6',
   '7', '8', '9',
   '*', '0', '#',
   0,   'E', 'S',
   'F', 'M', 0
};

const char STR_MESSAGES[] PROGMEM = "Messages";
const char STR_PHONEBOOK[] PROGMEM = "Phonebook";
const char STR_SETTINGS[] PROGMEM = "Settings";
const char STR_INBOX[] PROGMEM = "Inbox";
const char STR_SEND[] PROGMEM = "Send";
const char STR_DELETE[] PROGMEM = "Delete";
const char STR_NOT_IMPLEMENTED[] PROGMEM = "Not implemented";
const char STR_YES[] PROGMEM= "Yes";
const char STR_NO[] PROGMEM= "No";
const char STR_CONTRAST[] PROGMEM = "Contrast";
const char STR_BRIGHTNESS[] PROGMEM = "Brightness";
const char STR_LOW[] PROGMEM = "Low";
const char STR_MEDIUM[] PROGMEM = "Medium";
const char STR_HIGH[] PROGMEM = "High";


const menuItemType menu[] = {
  //id, parent, title, isEnabled, onSelect, parameter
   { 1,0, '1', STR_MESSAGES, NULL, NULL, 0 },
   { 2,0, '2', STR_PHONEBOOK, NULL, NULL, 0 },

   { 9,0, '9', STR_SETTINGS, NULL, NULL, 0 },
   
   { 10,1, '1', STR_INBOX, NULL, NULL, 0 },
   { 11,1, '2', STR_SEND, NULL, NULL, 0 },
   { 12,1, '3', STR_DELETE, NULL, NULL, 0 },
   { 13,1, '4', STR_NOT_IMPLEMENTED, NULL, NULL, 0 },
   
   { 20, 2, 0, STR_NOT_IMPLEMENTED, NULL, NULL, 0 },
   
   { 90, 9, 0, STR_CONTRAST, NULL, NULL, 0 },
   { 200, 90, 0, STR_LOW, NULL, onContrastMenuSelect, 0x00 },
   { 201, 90, 0, STR_MEDIUM, NULL, onContrastMenuSelect, 0x04 },
   { 202, 90, 0, STR_HIGH, NULL, onContrastMenuSelect, 0x08 },
   
   { 91, 9, 0, STR_BRIGHTNESS, NULL, NULL, 0 },
   { 210, 91, 0, STR_LOW, NULL, onBrightnessMenuSelect, 0x00 },
   { 211, 91, 0, STR_MEDIUM, NULL, onBrightnessMenuSelect, 0x01 },
   { 212, 91, 0, STR_HIGH, NULL, onBrightnessMenuSelect, 0x02 }
};

const uint8_t MENUITEMCOUNT = 16;

// global objects
class Phone : public HayesDelegate
{
   private:
    //Hayes gsm;
    //etc.
   public:
    void loop(); 
    void Hayes_CallingNumberUpdated(const char *data);
    void Hayes_SignalStrengthChanged(uint8_t signalStrength);
    void Hayes_ProviderNameUpdated(const char *data);
    void Hayes_StateChanged(ModemState oldState, ModemState newState);
    void Hayes_MessageReceived(int index);
};


ST7565r lcd(PIN_LCD_SI, PIN_LCD_CLOCK, PIN_LCD_A0, PIN_LCD_RESET, PIN_LCD_CS);
debugStream debugLine;
Hayes gsm;
ViewController viewController(&lcd);
Player ringtonePlayer(PIN_OUT_PWM_RINGER);
Keyboard keyboard(keyboardMatrix, 6, 3);
Phone phone;
MenuHandler menuHandler(menu, MENUITEMCOUNT);

uint8_t lastShifterValue = 0;
uint8_t missedCalls = 0;

bool inMenu = false;

const uint8_t NUMBERBUFFERSIZE = 20;

char numberBuffer[NUMBERBUFFERSIZE];
uint8_t numberBufferCount = 0;

timedEvent backlightTimer;

const unsigned int messageTone[] PROGMEM = 
{
   NOTE_C3, 32,
   NOTE_E3, 32,
   NOTE_G3, 32,
   NOTE_C4, 32,
   NOTE_E4, 32,
   NOTE_G4, 32,
   NOTE_C4, 8,
   0,0
 };

const unsigned int ringtone[] PROGMEM = 
{
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_F3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_F3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_E3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_E3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_DS3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_DS3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_E3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  NOTE_C3, 16,
  NOTE_GS3, 16,
  NOTE_G3, 16,
  NOTE_E3, 16,
  NOTE_G3, 16,
  NOTE_GS3, 16,
  
  0, 1,  //pause before looping.
  
  0, 0
};


/*********/
/* setup */
/*********/
void setup()
{
  pinMode(PIN_OUT_PWM_RINGER, OUTPUT);
  pinMode(PIN_IN_POWERBUTTON, INPUT_PULLUP);
  pinMode(PIN_ROAM, OUTPUT);
  pinMode(PIN_INUSE, OUTPUT);
  
  pinMode(PIN_OUT_GSM_POWER, OUTPUT);
  pinMode(PIN_OUT_PWM_RINGER, OUTPUT);
  pinMode(PIN_OUT_ENABLE_REGULATORS, OUTPUT);
  
  digitalWrite(PIN_ROAM, HIGH);
  digitalWrite(PIN_INUSE, HIGH); 
  
  Serial.begin(SERIAL_BAUDRATE);
  
  setupShifter();
  
  //power must be held 1000msecs for the phone to be turned on. 
  //this is a safeguard for pressing ON too short.
  unsigned long turnOn = millis();
  while(millis() - turnOn < POWER_TIMEOUT)
  {
    //do nothing but this.
  }
  
  //take over from the button and power the regulators.
  pinMode(PIN_OUT_ENABLE_REGULATORS, OUTPUT);
  digitalWrite(PIN_OUT_ENABLE_REGULATORS, HIGH);
  
  //When I press this little key, it plays a little melody (Kraftwerk)
  playTone(NOTE_A4, 90, 100);
  playTone(NOTE_A5, 90, 100);
  
  //boot.
  setupDebug();
  setupKeyboard();
  setupDisplay();
  setupGSM();
  
  numberBuffer[0] = 0;
  numberBufferCount = 0;

  viewController.displayMessage_P(PSTR("Boot done"));
  
  //boot melody
  playTone(NOTE_D4, 40, 50);
  playTone(NOTE_F4, 40, 50);
  playTone(NOTE_A4, 40, 50);
  playTone(NOTE_C4, 40, 50);
  playTone(NOTE_D5, 80, 80);

  digitalWrite(PIN_INUSE, LOW); 
}

bool onContrastMenuSelect(const menuItem *item)
{
  lcd.setContrast(item->menuParameter);
  return true;
}

bool onBrightnessMenuSelect(const menuItem *item)
{
  lcd.setPower(item->menuParameter);
  return true;
}

void playTone(int freq, int duration, int pause)
{
  tone(PIN_OUT_PWM_RINGER, freq, duration);
  if(pause > 0)
  {
    delay(pause);
  }
}

/*****************/
/* setup helpers */
/*****************/

void setupShifter()
{
  pinMode(PIN_SHIFTER_LATCH, OUTPUT);
  pinMode(PIN_SHIFTER_DATA, OUTPUT);  
  pinMode(PIN_SHIFTER_CLOCK, OUTPUT);
  sendShifter(0, 0xFF); //clear shifter
}

void setupDebug()
{
  pinMode(PIN_DEBUG_TX, OUTPUT);
  debugLine.print_P(PSTR("\n\nBoot SimplyCall 0.94 feb 2015\nDeveloped by Simplicate.info\n"));  
  debugLine.print_P(PSTR("Free ram: "));
  debugLine.println(freeMemory());
}

void setupKeyboard()
{
  debugLine.print_P(PSTR("Init Keyboard\n"));

  //we will be using the internal pull-up resistors. By pulling DOWN a shifter line we will
  //notice a keypress, while the other pins are high.
  
  pinMode(PIN_KEYBOARD_C0, INPUT);
  pinMode(PIN_KEYBOARD_C1, INPUT);
  pinMode(PIN_KEYBOARD_C2, INPUT);
}

void setupDisplay()
{
  debugLine.print_P(PSTR("Init Display\n"));
    
  enableBacklight(true);

  lcd.setup(0x00);
  lcd.update();
  
  viewController.setLowBattery(false);
  viewController.displayMessage_P(PSTR("SimplyCall"));
}

void setupGSM()
{
  debugLine.print_P(PSTR("Init GSM\n"));
  
  //from the guide:
  //Make sure that VBAT is stable before pulling down PWRKEY pin. The time between them is recommended 30ms.
  
  gsm.onDebug = &gsm_debug;
  gsm.delegate = &phone;
  gsm.setup(&Serial);
 
  //try three time to make a connection.
  for(int attempt = 0; attempt < 3; attempt++)
  {
    viewController.displayMessage_P(PSTR("INIT GSM"));
  
    debugLine.print_P(PSTR("Power cycle\n"));   
  
    //power up the GSM.
    digitalWrite(PIN_OUT_GSM_POWER, true);
    delay(2000);  //--delay needed to detect a power cycle. See datasheet.
    digitalWrite(PIN_OUT_GSM_POWER, false);
    
    if(gsm.connect())
    {
      return;
    }
    
//    playTone(NOTE_G1, 200, 0);
//    
//    viewController.displayMessage("GSM init");
//    debugLine.print_P(PSTR("GSM did not say hi\n"));   
//  
//    if(gsm.sendCommandBlocking("AT"))
//    {
//      debugLine.print_P(PSTR("GSM ok, configuring baudrate.\n")); 
//      if(gsm.sendCommandBlocking("AT+IPR=57600;&W"))
//      {
//          debugLine.print_P(PSTR("GSM baudrate configured.\n"));
//          return;
//      }
//      else
//      {
//          debugLine.print_P(PSTR("Configuration failed\n"));
//          viewController.displayMessage("GSM err#1");
//          
//          playTone(NOTE_G1, 500, 0);
//          delay(1000);
//      }
//    }
//    else
//    {
//      debugLine.print_P(PSTR("GSM no response from AT\n"));
//    }
  }
  
  //failed to setup the GSM.
  debugLine.print_P(PSTR("Init GSM failed after 3 attempts\n"));
  
  viewController.displayMessage_P(PSTR("GSM FAILED"));

  playTone(NOTE_G1, 100, 200);
  playTone(NOTE_G1, 100, 200);
  playTone(NOTE_G1, 100, 0);
            
  halt();
}

/*****************/
/* GSM callbacks */
/*****************/


void gsm_stateChanged(Hayes *hayes, ModemState newState)
{
  
}

void gsm_debug(Hayes *hayes, const char *msg)
{
  debugLine.print_P(PSTR("GSM: "));
  debugLine.println(msg); 
}


void halt()
{
  debugLine.print_P(PSTR("System Halt.\n"));
  enableRoam(false);
          
  //beep.
  while(true)
  {
  };
}

/**********************/
/* keyboard delegates */
/**********************/

bool getKeyboardValue(uint8_t row, uint8_t col)
{
   static uint8_t bitMask[] = { SHIFTER_ROW1, SHIFTER_ROW2, SHIFTER_ROW3, SHIFTER_ROW4, SHIFTER_ROW5, SHIFTER_ROW6 };
   static uint8_t columns[] = { PIN_KEYBOARD_C0, PIN_KEYBOARD_C1, PIN_KEYBOARD_C2 }; 
  
   //set shifter to enable correct row.
   sendShifter(bitMask[row], SHIFTER_ROW_MASK);
   bool value = digitalRead(columns[col]);
   
   return value;
}

void handleMainKeyboardEvent(char c)
{
  if(c >= '0' && c <= '9')
  {
    if(numberBufferCount < NUMBERBUFFERSIZE-1)
    {
      numberBuffer[numberBufferCount++] = c;
      numberBuffer[numberBufferCount] = 0;
      
      beepOk();
      
      viewController.displayMessage(numberBuffer);
    }
    else
    {
      //buffer full.
      beepWarning();
    }
  }
  else if(c == 'F')
  {
     //go to menu 
     menuHandler.reset();
     inMenu = true;
     viewController.setShowStatusBar(false);
     viewController.displayMenu(&menuHandler);
     
     beepOk();        
  }
  else if(c == 'E')  //DEL
  {
     if(missedCalls > 0)
     {
        //erase pending message before typing.
        missedCalls = 0; 
        beepOk();
     }
     else
     {
       if(numberBufferCount > 0)
       {
          numberBuffer[--numberBufferCount] = 0;
          
          viewController.displayMessage(numberBuffer);
          
          beepOk();
       }
       else
       {
         beepWarning();
       }
     }
  }
  else if(c == 'S') // SND
  {
     //Start the dial. Unsupported.
       viewController.displayMessage_P(PSTR("Not implemented"));
       beepWarning();        
  }
  else
  {
    //undefined key.
    beepError();   
  }
}

void handleMenuKeyboardEvent(char c)
{
   //it all depends on the current menu, if 'E' or 'F' should exit.
  if(c == 'E')
  {
     if(!menuHandler.selectBack())
     {
       //exit 
       inMenu = false;
       viewController.setShowStatusBar(true);
     }  
     else
     {
       viewController.displayMenu(&menuHandler);
     }
     beepOk();     
  }
  else if(c == 'F')
  {
    //perform next/down
    menuHandler.selectNext();
    viewController.displayMenu(&menuHandler);
    
    beepOk(); 
  }
  else if(c == 'S')  //send
  {
    //perform select
    menuHandler.selectCurrent(); 
    viewController.displayMenu(&menuHandler);
    
    beepOk();
  }
  else
  {
    //unknown key
    beepError();
  }
}

void onKeyboardPressed(char c)
{
  debugLine.print_P(PSTR("Key pressed: "));
  debugLine.println(c);
    
  enableBacklight(true); 
  
  //handle the actual keypress by delivering it to the view.
  switch(gsm.getModemState())
  {
    case ModemIdle:
      //add any pressed number to the dial number until the buffer is full.
      if(inMenu)
      {
        handleMenuKeyboardEvent(c);
      }
      else
      {
        handleMainKeyboardEvent(c);
      }
      break;
    case ModemIncomingCall:
      if(c == 'S') //SND
      {
        //take the call
        beepWarning();
      }
      else if(c == 'E') //END
      {
        gsm.hangUp();  
        viewController.displayMessage_P(PSTR("Call Blocked"));
        beepOk();  
      }
      else
      {
        beepError();  
      }
      break;
    case ModemCalling:
      if(c == 'E') //END, abort call.
      {
        gsm.hangUp();  
        viewController.displayMessage_P(PSTR("Call Ended"));
        beepOk();
      }
      else
      {
        beepError();
      }
      break;
//    case ModemInCall:
//      if(c == 'E') //END, abort call.
//      {
//        //TODO: Hand
//      }
//      else
//      {
//        playTone(NOTE_A3, 50, 100);   
//      }
//      break;
    default:
      viewController.displayMessage_P(PSTR("Unknown state"));
      beepError(); 
      break;
  }
}

/*********************/
/* main loop         */
/*********************/
void loop()
{
  static timedEvent inUseBlink;

  keyboard.update(&onKeyboardPressed, &getKeyboardValue);      //keyboard events will be handled by the delegates
  gsm.process();           //gsm responses will be handled by the delegates.
  checkPowerButton();
  updateBatteryStatus();
  updateBacklightTimer();
  ringtonePlayer.update();
  
  phone.loop();

  //we're done upcating the system, now just update the display and blink some lights. 
  viewController.updateDisplay();
 
  //blink the IN_USE light.
  if(inUseBlink.hasElapsed(2000U))
  {
    inUseBlink.reset();
    enableInUse(true);
  }
  else
  {
    enableInUse(false);
  }
  
  delay(1);
}
 
void checkPowerButton()
{
  static timedEvent onOffHeld;
  static bool powerHasBeenReleasedBefore = false;  //this variable blocks an issue when the power button is held after booting.  
  static bool pressed = false;

  //Powerbutton connects to ground and is default HIGH
  if(digitalRead(PIN_IN_POWERBUTTON) == LOW)
  {
    if(!pressed)
    {
        playTone(NOTE_A3, 50, 0);
        pressed = true;
    }
    
    if(powerHasBeenReleasedBefore)
    {
        enableRoam(true);
        enableBacklight(true);
      
        viewController.displayMessage_P(PSTR("Shutdown"));
    
       if(onOffHeld.hasElapsed(POWER_TIMEOUT))
       {
          debugLine.print_P(PSTR("System shutting down.\n"));
          viewController.displayMessage_P(PSTR("Bye."));
          
          //play a little melody.
          playTone(NOTE_D5, 40, 50);
          playTone(NOTE_C5, 40, 50);
          playTone(NOTE_A4, 40, 50);
          playTone(NOTE_F4, 40, 50);
          playTone(NOTE_D4, 100, 100);
          
          //we *ARE* shutting down now; turn everything off.
          digitalWrite(PIN_OUT_ENABLE_REGULATORS, LOW);
          enableBacklight(false);
          enableInUse(false);
          enableRoam(false);
        
          halt();
       }
    }
  }  
  else
  {
    powerHasBeenReleasedBefore = true;
    if(pressed)
    {
       viewController.displayMessage_P(PSTR(""));
    }
    pressed = false;
    
    onOffHeld.reset();
  }
};

/*********************/
/* auxiliary helpers */
/*********************/

void enableInUse(bool onOff)
{
  digitalWrite(PIN_INUSE, onOff?HIGH:LOW);
}

void enableRoam(bool onOff)
{
  digitalWrite(PIN_ROAM, onOff?HIGH:LOW);
}

void enableBacklight(bool onOff)
{
  sendShifter(onOff?SHIFTER_BACKLIGHT:0, SHIFTER_BACKLIGHT);
  if(onOff)
  {
     backlightTimer.reset(); 
     lcd.displayOn();
  }
  else
  {
     lcd.displayOff();
  }
}

void enableNoService(bool onOff)
{
  sendShifter(onOff?SHIFTER_NO_SERVICE:0, SHIFTER_NO_SERVICE);
}


void updateBacklightTimer()
{
  if(backlightTimer.hasElapsed(10000U))
  {
    enableBacklight(false);
  }
}

/***************************************/
/* clean up everything below this line */
/***************************************/

void sendShifter(uint8_t value, uint8_t mask)
{
  value = (lastShifterValue & ~mask) | value;
  
  digitalWrite(PIN_SHIFTER_LATCH, LOW);
  shiftOut(PIN_SHIFTER_DATA, PIN_SHIFTER_CLOCK, MSBFIRST, value);
  digitalWrite(PIN_SHIFTER_LATCH, HIGH);
  
  lastShifterValue = value;
}

void updateBatteryStatus()
{
  static timedEvent batteryCheck;
  static timedEvent batteryWarning;
  
  if(batteryCheck.hasElapsed(5000))
  {
     int voltage = getBatteryVoltage();
    
     viewController.setLowBattery(voltage < 110);
     
     if(voltage < 110)
     {
       //every minute.
       if(batteryWarning.hasElapsed(60*1000U))
       {
         viewController.displayMessage_P(PSTR("Low battery"));
         enableBacklight(true);
         playTone(NOTE_D2, 50, 100);
         playTone(NOTE_D1, 50, 0);
       }
     }
  }
}

//returns the battery voltage in 10ths of volts, e.g. 120 = 12.0 volts
unsigned int getBatteryVoltage()
{
  static unsigned int lastVoltage;
  
  //voltage 0..5 returns 0..1023
   int value = analogRead(PIN_IN_BATTERY_POWER);
   float readVoltage = (((float)value)*5.0)/1023;
 
   //vout = vin * (r2 / (r1 + r2))
   //--> vin = vout / (r2 / (r1 + r2))
   float factor = VBAT_R2 / (VBAT_R1 + VBAT_R2);
  
   float realVoltage = readVoltage/ factor;
   unsigned int returnVoltage = (int)(realVoltage * 10);
   
   if(returnVoltage != lastVoltage)
   {
     debugLine.print_P(PSTR("Battery voltage changed: "));
     debugLine.println(returnVoltage);
     lastVoltage = returnVoltage;
   }
   
   return returnVoltage;
}

void beepOk()
{
  playTone(NOTE_A3, 50, 0);    
}

void beepWarning()
{
  playTone(NOTE_A2, 50, 0);
}

void beepError()
{
   playTone(NOTE_A1, 250, 300); 
}

void Phone::loop()
{
   //move stuff here later.
}

void Phone::Hayes_CallingNumberUpdated(const char *data)
{  
  if(gsm.getModemState() == ModemIncomingCall)
  {
    viewController.displayMessage(data);
    enableBacklight(true);
  }
}

void Phone::Hayes_SignalStrengthChanged(uint8_t signalStrength)
{
  //hayes signal strength goes from 0..31, 99=undetermined.
  
  int strength = gsm.getSignalStrength();
  if(strength == 99)
  {
    strength = 0;
  }
  strength = strength / 3;
  if(strength > 5)
  {
    strength = 5;
  }
  
  viewController.setSignalStrength(strength);
}

void Phone::Hayes_ProviderNameUpdated(const char *data)
{
  viewController.setProviderName(gsm.getProviderName()); 
  enableBacklight(true);
  playTone(NOTE_C4, 10, 0);
}

void Phone::Hayes_MessageReceived(int index)
{
  //TODO: get the actual message for displaying?
  debugLine.print_P(PSTR("Incoming message, sir!\n"));
  
  ringtonePlayer.setMelody(messageTone, false);
  ringtonePlayer.play();
  
  enableBacklight(true);
}

void Phone::Hayes_StateChanged(ModemState oldState, ModemState newState)
{
  //bail out from the menu here if the state changes
  if(oldState == ModemIdle && inMenu == true)
  {
    inMenu = false;
    viewController.setShowStatusBar(true); 
    menuHandler.reset();
  }
  
  switch(newState)
  {
     case ModemIdle:
      if(oldState == ModemIncomingCall)
      {
         missedCalls++; 
      }
     
      if(missedCalls == 0)
      {
         viewController.displayMessage_P(PSTR("READY"));
      }
      else
      {
         char sbuf[30];
         sprintf_P(sbuf, PSTR("%d missed call"), missedCalls);
        
         viewController.displayMessage(sbuf);
      }
      
      enableBacklight(true); 
      ringtonePlayer.stop();
      break;
     case ModemIncomingCall:
      enableBacklight(true); 
      viewController.displayMessage_P(PSTR("RING RING"));
      
      ringtonePlayer.setMelody(ringtone, true);
      ringtonePlayer.play();
      break; 
     default:
      debugLine.print_P(PSTR("Something happened to the modem\n"));
      break;
   }
}

