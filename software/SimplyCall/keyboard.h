#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "debounce.h"

typedef void (*KeyboardEvent)(char key);
typedef bool (*KeyboardGetRowColumnValue)(uint8_t row, uint8_t col);

//class KeyboardDelegate
//{
//   public:
//     virtual void onKeyboardPressed(char key);
//     virtual bool onKeyboardGetColumnValue(uint8_t row, uint8_t col);
//};

class Keyboard
{
  private:
    uint8_t _rows;
    uint8_t _columns;
  
    const char *matrix;
    Debouncer keyDebounce;
    
  public:
     Keyboard(const char *matrix, uint8_t rows, uint8_t columns)
     {
        this->matrix = matrix;
        this->_rows = rows;
        this->_columns = columns;
        keyDebounce.setPressDelay(50);
     };
     
     void update(KeyboardEvent onKeyPressed, KeyboardGetRowColumnValue getValue)
     {
        for(int r = 0; r < _rows; r++)
        {
          for(int c = 0; c < _columns; c++)
          {
             bool rawValue = getValue(r,c);  
             
             if(rawValue)
             {
                char keyValue = matrix[(r * _columns) + c];

                //debounce that value.
                char key = keyDebounce.debounce(keyValue);
                if(key != 0)
                {
                  onKeyPressed(key);  
                }
                return;
             }
          }
        }  
        
        keyDebounce.debounce(0);
     };
};

#endif

