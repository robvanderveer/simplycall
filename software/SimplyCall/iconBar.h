#ifndef ICONBAR_H
#define ICONBAR_H

#include <arduino.h>

#define ICONBAR_BUFFERSIZE  10

//this helper class allows for dynamic adding of glyphs to a bar
class IconBar
{
  char buffer[ICONBAR_BUFFERSIZE];
  uint8_t count;
  
  public: 
    IconBar()
    { 
      count = 0;
      buffer[count] = '0';
    };
  
    void addGlyph(uint8_t glyph)
    {
        if(count < ICONBAR_BUFFERSIZE-1)
        {
          buffer[count++] = glyph;
          buffer[count] = '\0';
        }
    }
    
    void addGlyphIf(bool condition, uint8_t glyph)
    {
      if(condition)
      {
        addGlyph(glyph);
      } 
    }
    
    const char *getString()
    {
      return buffer;
    }
};

#endif

