
#include "timedEvent.h"

// this class debounces an input value by using a state machine and a few timers.
//
typedef enum DebounceState
{
    DebounceIdle,
    DebounceHolding,
    DebouncePressed,
    DebounceReleasing
} DebounceStateType;

class Debouncer
{
   private:
     int lastValue;
     DebounceStateType state;
     timedEvent timing;
     int pressDelay;
    
   public:
     Debouncer(int pressDelay)
     {
       this->pressDelay = pressDelay;
     };
     Debouncer()
     {
       this->pressDelay  = 100;
     };
     
     void setPressDelay(int delay)
     {
       this->pressDelay = delay;
     }
     
     int debounce(int value)
     {
        switch(state) 
        {
          case DebounceIdle:
            if(value != lastValue)
            {
              state = DebounceHolding;
              lastValue = value;
              timing.reset();
            }
            break;
          case DebounceHolding:
            if(value != lastValue)
            {
               state = DebounceIdle; 
            }
            else
            {
               if(timing.hasElapsed(pressDelay))
               {
                  state = DebouncePressed; 
                  return value;
               }
            }
            break;
          case DebouncePressed:
            if(value != lastValue)
            {
                state = DebounceReleasing;
            }
            else
            {   //key held.
           
            }
            break;
          case DebounceReleasing:
            if(value == lastValue)
            {
               state = DebouncePressed;
            }
            else
            {
              if(timing.hasElapsed(200u)) 
               {
                  state = DebounceIdle; 
               }
            }
            
            break;
        }
        
        return 0;
     };
};


