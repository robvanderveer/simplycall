#include "arduino.h"
#include "viewController.h"
#include "font_custom.h"
#include "glyphs.h"
#include "menu.h"

void ViewController::displayMessage(const char *message)
{
  lcd->fillRectangle(0, 0, 12, SCREEN_WIDTH, SCREEN_HEIGHT-12);
  lcd->print(message, 13, Tahoma14, 0, ALIGN_CENTER);
  lcd->update(); 
}

void ViewController::displayMessage_P(const PROGMEM char *message)
{
  char sbuf[32];
  
  strncpy_P(sbuf, message, 32);
  displayMessage(sbuf);
}

void ViewController::setProviderName(const char *name)
{
   strlcpy(providerNameBuf, name, TITLEBARSIZE); 
}

// handle animations, refresh the display.
void ViewController::updateDisplay()
{
  //perform the blinking logic switch.
  if(blinker.hasElapsed(500))
  {
     blinkOn = !blinkOn; 
     blinker.reset();
  }  
      
  if(isShowingStatusBar)
  {
    //blank the statusbar. 
    lcd->fillRectangle(0, 0, 0, SCREEN_WIDTH, 12);
     
    //draw the level glyphs
    switch(signalStrength)
    {
    case 0:
      if(blinkOn)
      {
        lcd->print("ABCDE", 0, 0, glyphs, 0);
      }
      break;
    case 1:
      lcd->print("AB", 0, 0, glyphs, 0);
      break;
    case 2:
      lcd->print("AB", 0, 0, glyphs, 0);
      break;
    case 3:
      lcd->print("ABC", 0, 0, glyphs, 0);
      break;
    case 4:
      lcd->print("ABCD", 0, 0, glyphs, 0);
      break;
    default:
      lcd->print("ABCDE", 0, 0, glyphs, 0);
      break;
    }
    
    //draw the provider name.
    lcd->print(providerNameBuf, 0, Verdana8, 0, ALIGN_CENTER);
    
    //draw the battery indicator. Blink if very low.
    IconBar bar;
    bar.addGlyphIf(lowBattery && blinkOn, GLYPH_LOWBATT);
    bar.addGlyphIf(unreadMessages > 0, GLYPH_MSG);
    lcd->print(bar.getString(), 0, glyphs, 0, ALIGN_RIGHT);
  }
  
   lcd->update();
}

void ViewController::setSignalStrength(uint8_t level)
{
  this->signalStrength = level;
  if(level == 0)
  {
    this->blinkOn = true;
    this->blinker.reset();
  }
}

// activeParent is the ID to show the submenus, hoverId is the item that currently has 'focus'
void ViewController::displayMenu(MenuHandler *menuHandler)
{
  //find the amount of child menu's for the current menu.
//  uint8_t itemCount = 0;
//  uint8_t activeIndex = 0;
//  for(uint8_t i = 0; i < _numItems; i++)
//  {
//    if(menus[i].parentId == activeParent)
//    {
//      itemCount++;
//    }
//    if(menus[i].menuId == hoverId)
//    {
//      activeIndex = itemCount;
//    }
//  }

  //clear the screen.
  lcd->clear();

  uint8_t activeIndex = 0;
  uint8_t itemsToDisplay = menuHandler->getNumberOfChildItems(&activeIndex);
  
  //draw max 3 rows.
  //center around the active menu-items.
  uint8_t top = 0;  //top item to display.
  if(itemsToDisplay > 3) 
  {
    //we selected the last one.
    if(itemsToDisplay-1 == activeIndex)
    {
      top = itemsToDisplay - 2;
    }
    //or the first one.
    else if(activeIndex == 0)
    {
      top = 0;
    }
    //otherwise always in the middle.
    else
    {
      top = activeIndex - 1;
    }
  }

  if(top > 0)
  {
    lcd->print(GLYPH_UP, SCREEN_WIDTH - 30, 0, glyphs, 0);
  }
  if(top < itemsToDisplay - 3)
  {
    lcd->print(GLYPH_DOWN, SCREEN_WIDTH - 30, SCREEN_HEIGHT-9, glyphs, 0);
  }

  int rowHeight = 11;
  
  for(uint8_t row = 0; row < 3 && row < itemsToDisplay; row++)
  {
    const menuItemType *item = menuHandler->getChild(row+top);
    if(item->glyph != 0)
    {
      lcd->print(item->glyph, 0, row * rowHeight, numberglyphs, 0);
    }
    
    if(row+top == activeIndex)
    {
      //reverse.
      lcd->fillRectangle(1, 8, row * rowHeight, SCREEN_WIDTH - 30 - 8, rowHeight+1);
      lcd->printP(item->title, 1+8, row * rowHeight, Verdana8, 1); 
    }
    else
    {
      lcd->printP(item->title, 1+8, row * rowHeight, Verdana8, 0); 
    } 
  }

  //print 'END'
  lcd->print(GLYPH_END, SCREEN_WIDTH-17, SCREEN_HEIGHT-9, glyphs, 0);
  lcd->update();
}

