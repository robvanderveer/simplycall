#include "ST7565r.h"
#include "menu.h"
#include "timedEvent.h"
#include "iconBar.h"

#ifndef VIEWCONTROLLER
#define VIEWCONTROLLER

const uint8_t TITLEBARSIZE = 20;

class ViewController
{
  private:
    ST7565r *lcd;
    bool lowBattery;
    char providerNameBuf[TITLEBARSIZE];
    uint8_t signalStrength;
    uint8_t unreadMessages;
    bool isShowingStatusBar;
    
    //animation helpers
    timedEvent blinker;
    bool blinkOn;
    
  public:
    ViewController(ST7565r *lcd)
    {
      this->lcd = lcd;
      strlcpy_P(providerNameBuf, PSTR("no carrier"), TITLEBARSIZE);
      this->lowBattery = true;
      this->signalStrength = 0;
      this->unreadMessages = 0;
      this->isShowingStatusBar = true;
    };
    
    void updateDisplay();
    void setSignalStrength(uint8_t level); //0..4
    void setProviderName(const char *name);
    void setLowBattery(bool low)
    {
      this->lowBattery = low;
    };
    void setUnreadMessages(int number)
    {
      this->unreadMessages = number;
    };
    void setShowStatusBar(bool show)
    {
      if(show != isShowingStatusBar)
      {
        lcd->clear();
        isShowingStatusBar = show;
      }
    };
    void displayMessageWithTitle(const char *title, const char *message);
    void displayMessage(const char *message);    //display a message with the normal status bar, also overrides any DisplayMessageWithTitle
    void displayMessage_P(const PROGMEM char *message);
    void displayMenu(MenuHandler *menuHandler);
};

#endif

