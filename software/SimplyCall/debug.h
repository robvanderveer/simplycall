#include <arduino.h>

//use hardware port 13 (PB5) for max performance.
class debugStream
{
    public:
      debugStream()
      {
        writePin(HIGH);
      }

      void print_P(const PROGMEM char *str)
      {
        char c;
        if(!str) return;
        while((c = pgm_read_byte(str++)))
        {
          writeByte(c);
        }
      };
      
      void println(const char *line)
      {
        print(line);
        writeByte('\r');
        writeByte('\n');
      };
      
      void print(const char *line)
      {
        while(*line != 0)
        {
          writeByte(*line);
          line++;
        }
      };
      void println(int i)
      {
        char sbuf[10];
        sprintf(sbuf, "%d", i);
        println(sbuf);
      };
      
      //http://www.dnatechindia.com/Tutorial/8051-Tutorial/BIT-BANGING.html
      void writeByte(uint8_t c)
      {
         //int periodMicroSecs = 104; //1/9600  interval
         int periodMicroSecs = 26;  //1/38400 interval
         
         writePin(LOW);
         delayMicroseconds(periodMicroSecs);
         for(uint8_t b = 0; b < 8; b++)
         {
           writePin(c & 0x01);
           c >>= 1;
           delayMicroseconds(periodMicroSecs);
         }
         
         //stop bit
         writePin(HIGH);
         delayMicroseconds(periodMicroSecs*2);
      };
      
      void writePin(bool value)
      {
        if(value)
        {
          PORTB |= 0b00100000;
        }
        else
        {
          PORTB &= ~0b00100000;
        }
      };
};

