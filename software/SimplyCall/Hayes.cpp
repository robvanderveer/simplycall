#include <Time.h>

#include "Hayes.h"

Hayes::Hayes()
{
  this->_modemState = ModemInitializing; 
}

bool Hayes::setup(Stream *stream)
{
  this->stream = stream;
  this->_commandState = CommandIdle;
  this->_modemState = ModemInitializing;
  this->responseBufferPos = 0;
  this->pinUnlocked = true;
  this->signalStrength = 0;
  strlcpy_P(this->providerNameBuf, PSTR(""), PROVIDERNAMESIZE);
  
  return true;
}

bool Hayes::connect()
{
    timedEvent timeout;
    timeout.reset();
    
    //if autobaud has been disable (AT+IPR=0) in the default configuration, the URC "RDY" is not sent after
    //powering on. AT command can be send 2-3 seconds after the module is powered on. Only AT or at can be sent
    //until receiving "OK" from module. Then an "AT+IPR=x;&W" should be sent to set a fixed baudrate for the module.
    
    //in other words, after power up:
    //if we do not get a "RDY" within 2 seconds, send AT to confirm and write the baudrate.
    //if we DO get a RDY, we're good to go anyway.
    //if we get garbage. The baudrate is set wrong and we need to adjust manually.
    while(!timeout.hasElapsed(4000U))
    {
      process();
  
      if(_modemState == ModemIdle)
      {
         if(sendCommandBlocking("AT+CLIP=1;+CNMI=2,1;"))
         {
           return true;
         }
         else
         {
           debug_P(PSTR("init failed"));
           return false; 
         }
      }  
    }
    
    return false;
}

void Hayes::debug(const char *msg)
{
  if(this->onDebug)
  {   
    this->onDebug(this, msg);
  }
}

void Hayes::debug_P(const PROGMEM char *msg)
{
  if(this->onDebug)
  {   
    char sbuf[60];
    strncpy_P(sbuf, msg, 60);
    this->onDebug(this, sbuf);
  }
}

void Hayes::setModemState(ModemState state)
{
   if(state != _modemState)
   {
     ModemState oldState = _modemState;

     _modemState = state;
     if(delegate)
     {
       delegate->Hayes_StateChanged(oldState, state);
     }
   }
}

bool Hayes::processSignalStrength(const char *data)
{
  int strength;
  int dummy;
  bool changed;
  
  //TODO: refactor this, because parsing the input buffer is bad.
  if(sscanf_P(data, PSTR("+CSQ: %d,%d"), &strength, &dummy) == 2)
  {
     //only notify a change
     if(this->signalStrength != strength)
     {
        this->signalStrength = strength;
        if(this->delegate)
        {
           this->delegate->Hayes_SignalStrengthChanged(strength);
        }
     }
     return true;
  }
  
  char providerBuf[20];
  if(sscanf_P(data, PSTR("+COPS: %d,%d,\"%[^\"]\""), &strength, &dummy, providerBuf) == 3)
  {
     //only notify a change
     if(strcmp(this->providerNameBuf, providerBuf) != 0)
     {
       strlcpy(this->providerNameBuf, providerBuf, PROVIDERNAMESIZE);
       if(this->delegate)
       {
          this->delegate->Hayes_ProviderNameUpdated(this->providerNameBuf); 
       }
     }
     return true;
  }
  
  if(strcmp_P(data, PSTR("+COPS: 0")) == 0)
  {
     if(strcmp_P(this->providerNameBuf, PSTR("no provider")) != 0)
     {
       strlcpy_P(this->providerNameBuf, PSTR("no provider"), PROVIDERNAMESIZE);
       if(this->delegate)
       {
          this->delegate->Hayes_ProviderNameUpdated(this->providerNameBuf); 
       }
     }
     return true;
  }
  
  return false;
} 
 
//  The time parsing code below works, but is basically useless as the
//  modem does not support getting the time from the network.
//bool processClock(const char *data)
//{  
//  int y,M,d,h,m,s;
//  char zone[2];
//  char offset[6];
//  
//  //TODO: refactor this, because parsing the input buffer is bad.
//  if(sscanf_P(responseBuffer, PSTR("+CCLK: \"%d/%d/%d,%d:%d:%d%1[-+]%6[0123456789]\""), &y, &M, &d, &h, &m, &s, &zone, &offset) > 0)
//  {
//     setTime(h,m,s,d,M,y); 
//     debug("parsed time");
//     updateStateChange();
//     return true;
//  }
//  return false;
//}
    
/// call regulary to process the incoming data
void Hayes::process()
{
  //kill a command if it has timed out.
  if(isBusy() && commandTimeout.hasElapsed(2000U, false))
  {
    debug_P(PSTR("timeout"));

    _commandState = CommandComplete;
    didTimeOut = true;

    //TODO handle the error through the delegate.
  } 
 
  //eat input.
  while(stream->available())
  {
    //add the input to the buffer until newline.
    char data = stream->read();
    if(data == '\n')
    {
      //ignore newlines
      continue;
    }
    
    if(responseBufferPos > BUFFERSIZE-2)
    {
      debug_P(PSTR("Overflow"));
      responseBufferPos = 0;
    }
 
    responseBuffer[responseBufferPos++] = data;
    responseBuffer[responseBufferPos] = '\0';
    
    //if the buffer ends with \r process the input.
    if(responseBufferPos > 0)
    {
      if(responseBuffer[responseBufferPos - 1] == '\r')
      {
        //remove the line ending.
        responseBuffer[responseBufferPos - 1] = '\0';
        
        //process the input; need to reset the buffer BEFORE processing.
        responseBufferPos = 0;
        
        processResponse(responseBuffer);
      }
    }
  }
  
  //update signal strength and provider name chain.
  if(_modemState == ModemIdle && updateSignalTimer.hasElapsed(10000U))
  {
    if(!isBusy())
    {
      sendCommand("AT+CSQ;+COPS?", &Hayes::processSignalStrength);   
    }
  }
}

void Hayes::processResponse(const char *input)
{
  //skip the empty lines.
  if(!strcmp(input, ""))
  {
    //no need to debug this.
    //debug("response", ""); 
    return;
  }
  
  switch(_commandState)
  {
     case CommandWaiting:
        if(strcmp(input, lastCommand) == 0)
        {
          //awesome.
          _commandState = CommandConfirmed;
        }
        else
        {
          debug_P(PSTR("no match, trying URC"));
          handleURC(input);
        }
        break;
    case CommandConfirmed:
       if(strcmp_P(input, PSTR("OK")) == 0)
       {
         //TODO: Handle OK.
         _commandState = CommandComplete;
         return;
       }
       else if(strcmp_P(input, PSTR("ERROR")) == 0)
       {
         _commandState = CommandComplete;
         
         debug_P(PSTR("response ERROR"));
         //TODO: handleError();
         return;
       }
       else
       {
         //we received other data. Buffer the response and carry on
         if(this->onReceiveData)
         {
            if(!(this->*onReceiveData)(input))
            {
              debug_P(PSTR("failed to parse"));
              debug(input);
            }
         }
       }
       break; 
    case CommandIdle:
    case CommandComplete:
      handleURC(input);
      break;
  }
}

bool Hayes::handleURC(const char *command)
{
  char sbuf[20];
  
  if(strcmp_P(command, PSTR("RDY")) == 0)
  {
    //modem is ready to accept commands.
    debug_P(PSTR("Modem is ready for commands"));
    
    updateSignalTimer.reset();
    setModemState(ModemIdle);
    return true;
  }
  
  if(strcmp_P(command, PSTR("Call Ready")) == 0)
  {
    //TODO: modem is ready to make/receive calls.
    return true;
  }
  
  if(strcmp_P(command, PSTR("RING")) == 0 ||
     strcmp_P(command, PSTR("+CRING: VOICE")) == 0)
  {
    //call
    setModemState(ModemIncomingCall);
    return true;
  }
  
  if(strcmp_P(command, PSTR("BUSY")) == 0)
  {
    //TODO
    return true;
  }
  
  if(strcmp_P(command, PSTR("NO CARRIER")) == 0)
  {
     //hung up/call dropped
     setModemState(ModemIdle);
     return true;
  }
  
  if(strcmp_P(command, PSTR("+CPIN: READY")) == 0)
  {
     //cool, no PIN
     pinUnlocked = true;
     
     //TODO, signal PIN unlocked.
     return true;
  }

  //  AT+CLIP=1 : response +CLIP: "+31650698264",145,"",,"",0
  int dummy0, dummy1;
  if(sscanf_P(command, PSTR("+CLIP: \"%[^\"]\",%d,\"\",,\"\",%d"), sbuf, &dummy0, &dummy1) > 0)
  {
    strlcpy(callingNumberBuf, sbuf, PROVIDERNAMESIZE);
    if(delegate)
    {
       delegate->Hayes_CallingNumberUpdated(callingNumberBuf);
    }
    return true;
  }
  
  int index;
  // example: +CMTI: "SM",10
  if(sscanf_P(command, PSTR("+CMTI: \"%[^\"]\",%d"), sbuf, &index) > 0)
  {
    if(delegate)
    {
       delegate->Hayes_MessageReceived(index);
    }
    return true;
  }
  
  //TODO: Handle more URCs here
  
   debug_P(PSTR("unknown urc"));
   debug(command);
   return false;
}

bool Hayes::sendCommandBlocking(const char *command)
{
  if(sendCommand(command, NULL))
  {
     //keep processing until the answer comes in.
     while(_commandState != CommandComplete)
     {
        this->process();  
     } 
     
     //TODO: parse the response before returning
     if(didTimeOut) 
     {
       return false;
     }
     
     return true;
  }
  return false;
}

//note:  If an event which delivers a URC coincides with the execution of an AT command, the URC will be output after command execution has completed.
bool Hayes::sendCommand(const char *command, HayesCallback receivedData)
{
  if(isBusy())
  {
    //can't have this.
    debug_P(PSTR("BUSY"));
    return false;
  }
  
  this->onReceiveData = receivedData;
  
  strlcpy(lastCommand, command, COMMANDSIZE);
  stream->print(lastCommand);
  stream->print('\r');
  
  commandTimeout.reset();    //start counting now.
  _commandState = CommandWaiting;
  didTimeOut = false;
  
  return true;
}

const char *Hayes::getCallingNumber()
{
  return callingNumberBuf;
}

void Hayes::hangUp()
{
  //process a busy command.
  while(isBusy())
  {
    process();
  }
  
  sendCommandBlocking("ATH0");
  
  //the connection will be terminated and the URC NO CARRIER will update the state.
  setModemState(ModemIdle);
}

//bool Hayes::dialNumber(char *number, HayesDelegate *completion)
//{
//   if(!isCallReady)
//   {
//      return false;
//   }
//   if(sendCommand("ATDTblablabla", NULL))
//   {
//     //OK,BUSY or ERROR messages are handled elsewhere.
//     //currentHandler = completion;
//   } 
//}

