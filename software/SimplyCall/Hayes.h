#include "timedEvent.h"

const unsigned int BUFFERSIZE = 64;
const unsigned int COMMANDSIZE = 32;
const unsigned int PROVIDERNAMESIZE = 20;

typedef enum 
{
   ModemInitializing,
   ModemIdle,
   ModemIncomingCall,
   ModemCalling,
   ModemError
} ModemState;

typedef enum
{
   CommandIdle,
   CommandWaiting,
   CommandConfirmed,
   CommandComplete
} CommandState;

//forward declaration
class Hayes;

//callback function, return false when you cannot parse the response.
typedef bool (Hayes::*HayesCallback)(const char *data);

//public hooks
typedef void (*HayesDebugDelegate)(Hayes *system, const char *msg);

class HayesDelegate
{
   public:
     virtual void Hayes_CallingNumberUpdated(const char *data);
     virtual void Hayes_SignalStrengthChanged(uint8_t signalStrength);
     virtual void Hayes_ProviderNameUpdated(const char *data);
     virtual void Hayes_StateChanged(ModemState oldState, ModemState newState);
     virtual void Hayes_MessageReceived(int index);
};

class Hayes
{
  private:
    unsigned int responseBufferPos;
    Stream *stream;
    HayesCallback onReceiveData;
    
    ModemState _modemState;
    CommandState _commandState;
    bool pinUnlocked;
    bool didTimeOut;
    uint8_t signalStrength;

    bool handleURC(const char *command);
    void debug(const char *msg);
    void debug_P(const PROGMEM char *msg);
    void updateStateChange();
    
    timedEvent commandTimeout; 
    timedEvent updateSignalTimer;
    
    char responseBuffer[BUFFERSIZE];
    char lastCommand[COMMANDSIZE];    //is this big enough?
    char providerNameBuf[PROVIDERNAMESIZE];
    char callingNumberBuf[PROVIDERNAMESIZE];
    
    /* internal callback methods */
    bool processSignalStrength(const char *data);
  public:
    Hayes();
    bool setup(Stream *stream);
    bool connect();
    void process();  //process incoming data
    void processResponse(const char *input);
    
    HayesDelegate *delegate;
    HayesDebugDelegate onDebug;
//    HayesStateChangedDelegate onStateChanged;
    
    //properties
    ModemState getModemState()
    {
       return _modemState; 
    }
    void setModemState(ModemState state);
    bool isBusy()
    {
      return _commandState == CommandWaiting || _commandState == CommandConfirmed;
    } 
    uint8_t getSignalStrength()
    {
      return signalStrength;
    }
    const char *getProviderName()
    {
      return providerNameBuf;
    }
    
    bool sendCommandBlocking(const char *command);
    bool sendCommand(const char *command, HayesCallback completion);
    const char *getCallingNumber();
    void hangUp();
};

