#include "Hayes.h"

Hayes::Hayes()
{
  this->responseBufferPos = 0;
  this->isCallReady = false;
  this->didTimeOut = false;
}

bool Hayes::setup(Stream *stream)
{
  debug("setup", "init");
  this->stream = stream;
  
  return true;
}

void Hayes::debug(const char *intro, const char *msg)
{
  if(this->onDebug)
  {
    sprintf(debugBuf, "%s: [%s]", intro, msg);
    (*this->onDebug)(this, debugBuf);
  }
}
    
/// call regulary to process the incoming data
void Hayes::process()
{
  //kill a command if it has timed out.
  if(isCommandBusy)
  {
    if(commandTimeout.hasElapsed(5000U, false))
    {
      debug("command timed out", lastCommand);

      isCommandBusy = false;
      didTimeOut = true;
      
      //TODO: the command failed because of a timeout. Tell the commandhandler to abort.
      if(currentHandler != NULL)
      {
        currentHandler->handleError();
        delete currentHandler;
      }

      currentHandler = NULL;
    }
  } 

  //eat input.
  while(stream->available())
  {
    //add the input to the buffer until newline.
    char data = stream->read();
    if(data == '\n')
    {
      //ignore newlines
      continue;
    }
    
    if(responseBufferPos > 250)
    {
      debug("ERROR", "Buffer overflow");
      responseBufferPos = 0;
    }
 
    responseBuffer[responseBufferPos++] = data;
    responseBuffer[responseBufferPos] = '\0';
    
    debug("state", responseBuffer);
    
    //if the buffer ends with \r process the input.
    if(responseBufferPos > 0)
    {
      if(responseBuffer[responseBufferPos - 1] == '\r')
      {
        //remove the line ending.
        responseBuffer[responseBufferPos - 1] = '\0';
        
        //process the input; need to reset the buffer BEFORE processing.
        responseBufferPos = 0;
        
        processResponse(responseBuffer);
      }
    }
  }
}

void Hayes::processResponse(const char *input)
{
  //skip the empty lines.
  if(!strcmp(input, ""))
  {
    //no need to debug this.
    //debug("response", ""); 
    return;
  }
  
  debug("incoming", input);

  //hayes will never interrupt a command with an unsollicited response
  //so if a handler has been set up, it should process the answer. Otherwise it is an URC
  if(this->currentHandler)
  {
    if(!didCommandMatch)
    {
      if(strcmp(input, lastCommand) == 0)
      {
        //awesome.
        debug("command", "match");
        didCommandMatch = true;
      }
      else
      {
        handleURC(input);
      }
    }
    else
    {
      if(currentHandler->handleResponse(input))
      {
        delete currentHandler;
        currentHandler = NULL;
        isCommandBusy = false;
        debug("command complete", "");
      }
      else
      {
        //input was not understood.
        debug("respone err", input);
      }
    }
  }
  else
  {
    handleURC(input);
  }
}

bool Hayes::handleURC(const char *command)
{
  if(strcmp(command, "Call Ready") == 0)
  {
    isCallReady = true;
    updateStateChange(true);
    return true;
  }
  
  if(strcmp(command, "RDY") == 0)
  {
    isCallReady = true;
    updateStateChange(true);
    return true;
  }
  
  //TODO: Handle more URCs here
  
   debug("urc", command);
   return false;
}

bool Hayes::sendCommandBlocking(const char *command)
{
  HayesResponseHandler *handler = new HayesResponseHandler(this, NULL);
  
  if(sendCommand(command, handler))
  {
     //keep processing until the answer comes in.
     while(isCommandBusy)
     {
        this->process();  
     } 
     
     //TODO: parse the response before returning
     if(didTimeOut) 
     {
       return false;
     }
     
     return true;
  }
  return false;
}

//note:  If an event which delivers a URC coincides with the execution of an AT command, the URC will be output after command execution has completed.
bool Hayes::sendCommand(const char *command, HayesResponseHandler *responseHandler)
{
  if(isCommandBusy)
  {
    //can't have this.
    debug("sendCommand BUSY", lastCommand);
    return false;
  }
  
  this->currentHandler = responseHandler;
  
  strcpy(lastCommand, command);
  stream->println(lastCommand);
  
  commandTimeout.reset();    //start counting now.
  isCommandBusy = true;
  didCommandMatch = false;
  didTimeOut = false;
  
  debug("command sent", lastCommand);
  return true;
}

void Hayes::updateStateChange(bool success)
{
   if(this->onStateChanged)
   {
     (*this->onStateChanged)(this, success);
   }
}

//bool Hayes::dialNumber(char *number, HayesDelegate *completion)
//{
//   if(!isCallReady)
//   {
//      return false;
//   }
//   if(sendCommand("ATDTblablabla", NULL))
//   {
//     //OK,BUSY or ERROR messages are handled elsewhere.
//     //currentHandler = completion;
//   } 
//}

