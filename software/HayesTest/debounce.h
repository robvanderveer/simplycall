
#include <timedEvent.h>

// this class debounces an input value by using a state machine and a few timers.
//
typedef enum DebounceState
{
    Idle,
    Holding,
    Pressed,
    Releasing
};

class Debouncer
{
   private:
     int lastValue;
     DebounceState state;
     timedEvent timing;
    
   public:
     int debounce(int value)
     {
        switch(state) 
        {
          case Idle:
            if(value != lastValue)
            {
              state = Holding;
              lastValue = value;
              timing.reset();
            }
            break;
          case Holding:
            if(value != lastValue)
            {
               state = Idle; 
            }
            else
            {
               if(timing.hasElapsed(100u))
               {
                  state = Pressed; 
                  return value;
               }
            }
            break;
          case Pressed:
            if(value != lastValue)
            {
                state = Releasing;
            }
            else
            {   //key held.
           
            }
            break;
          case Releasing:
            if(value == lastValue)
            {
               state = Pressed;
            }
            else
            {
              if(timing.hasElapsed(200u)) 
               {
                  state = Idle; 
               }
            }
            
            break;
        }
        
        return 0;
     };
};

