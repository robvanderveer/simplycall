/*
 * Hayes test
 */
 
#include <SoftwareSerial.h>
#include "Hayes.h"
#include "timedEvent.h"
#include "debounce.h"

Hayes myHayes;
SoftwareSerial debugLine(0,13);
Debouncer keyboardDebounce;

const uint8_t DATA = 2;    //PD2
const uint8_t CLOCK = 4;    //PD4
const uint8_t LATCH = 3;    //PD3

const uint8_t KEYBOARD_C0 = 9;
const uint8_t KEYBOARD_C1 = 10;
const uint8_t KEYBOARD_C2 = 11;

// power section
const uint8_t PIN_OUT_PWM_RINGER = 5;
const uint8_t PIN_OUT_GSM_POWER = 6;
const uint8_t PIN_IN_POWERBUTTON = 7;
const uint8_t PIN_OUT_ENABLE_REGULATORS = 8;
const uint8_t PIN_IN_BATTERY_POWER = 14;

void setup()
{
  debugLine.begin(57600);
  debugLine.println("Boot");
  
  pinMode(PIN_OUT_ENABLE_REGULATORS, OUTPUT);
  pinMode(PIN_OUT_GSM_POWER, OUTPUT);
  pinMode(PIN_OUT_PWM_RINGER, OUTPUT);
//  digitalWrite(PIN_OUT_ENABLE_REGULATORS, false);
//  delay(500);
  digitalWrite(PIN_OUT_ENABLE_REGULATORS, true);
  delay(500);
  
  keyboardSetup();
  
  Serial.begin(57600);
  myHayes.onDebug = &hayes_debug;
  myHayes.setup(&Serial);
  
  //power up the GSM.
  debugLine.println("Power high");
   digitalWrite(PIN_OUT_GSM_POWER, true);
    delay(2000);  //--delay needed to detect a power cycle. See datasheet.
    digitalWrite(PIN_OUT_GSM_POWER, false);
    delay(500);
  
  sendShifter(0);
  myHayes.sendCommandBlocking("AT"); //replace this by myHayes.connect
  //delay(1000);
  //Serial.println("AT+IPR=19200");
  //delay(1000);
  
  //myHayes.sendCommandBlocking("ATE1;+CLIP=1;+CRC=1;");
  delay(100);
}

int counter = 0;
unsigned long wait = 0;

void loop()
{
  myHayes.process();
  
  //keyboardScan();
  
  if(millis() - wait > 5000U)
  {
    wait = millis();
    //counter++;
    //sendShifter(counter);
   
    myHayes.sendCommand("AT", new HayesResponseHandler(&myHayes, NULL)); 
  }
}

void sendShifter(uint8_t value)
{
  digitalWrite(LATCH, LOW);
  shiftOut(DATA, CLOCK, MSBFIRST, value);
     //delay(50);
  digitalWrite(LATCH, HIGH);
}

void hayes_debug(Hayes *hayes, char *msg)
{
  debugLine.print("##: ");
  debugLine.println(msg); 
}

/* Keyboard prototype logic below this line */
/* ---------------------------------------- */

char keyboardMatrix[] = 
{
   '1', '2', '3',
   '4', '5', '6',
   '7', '8', '9',
   '*', '0', '#',
   0,   'E', 'S',
   'F', 'M', 0
};

void keyboardSetup()
{
  //we will be using the internal pull-up resistors. By pulling DOWN a shifter line we will
  //notice a keypress, while the other pins are high.
  
  pinMode(KEYBOARD_C0, INPUT);
  pinMode(KEYBOARD_C1, INPUT);
  pinMode(KEYBOARD_C2, INPUT);
  
  //the OUTPUT pins are done by the shifter, so no output necessary.
  debugLine.println("keyboard setup");
}

//for fastest operation, do one row per callback. For now, we just loop everything
char keyboardScan()
{
  static uint8_t cols[] = { KEYBOARD_C0, KEYBOARD_C1, KEYBOARD_C2 };
  static uint8_t rowBits[] = { 2, 4, 8, 16, 32, 64 };
  
  int currentKey = 0;
  bool found = false;
  for(int row = 0; row < 6 && !found; row++)
  {
    //set the shifter, topmost bits, use a negative mask.
    sendShifter(rowBits[row]);
    delay(1);
    
    for(int col = 0; col < 3 && !found; col++)
    {
      bool keyPress = digitalRead(cols[col]);
      if(keyPress)
      {
        currentKey = keyboardMatrix[(row * 3) + col];
        found = true;
      }
    }
  }
  
  currentKey = keyboardDebounce.debounce(currentKey);
  if(currentKey != 0)
  {  
    debugLine.print("KEYPRESS: ");
    debugLine.println((char)currentKey);    
  }
}

