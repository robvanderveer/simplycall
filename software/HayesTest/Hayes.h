#include <arduino.h>
#include "timedEvent.h"

const unsigned int BUFFERSIZE = 256;

//forward declaration
class Hayes;

//callback function
typedef void (*HayesDelegate)(Hayes *system, bool succes);
typedef void (*HayesDebug)(Hayes *system, char *msg);

class HayesResponseHandler
{
  protected:
   HayesDelegate *delegate;
   Hayes *hayes;
   
  public:
   HayesResponseHandler(Hayes *hayes, HayesDelegate *delegate)
   {
     this->hayes = hayes;
     this->delegate = delegate;
   };
  
   bool handleResponse(const char *response)
   {
     // this is just the basic OK/ERROR handling. Override if you need more.
     // return true when the command has finished processing all data.
     if(strcmp(response, "OK") == 0)
     {
       return true;
     }
     
     if(strcmp(response, "ERROR") == 0)
     {
       handleError();
       return true;
     }
     
     //i have not understood this message.
     return false;
   };
   void handleError()
   {
     if(delegate)
     {
       (*delegate)(hayes, false);
     }
   };
};

class Hayes
{
  private:
    unsigned int responseBufferPos;
    Stream *stream;
    bool isInCall;          //are we processing a call
    bool isCallReady;       //did the modem tell us we are good to go?
    bool isCommandBusy;
    bool didCommandMatch;
    bool didTimeOut;
    HayesResponseHandler *currentHandler;
    bool handleURC(const char *command);
    void debug(const char *intro, const char *msg);
    void updateStateChange(bool success);
    timedEvent commandTimeout;
    
    char responseBuffer[BUFFERSIZE];
    char lastCommand[80];    //is this big enough?
    char debugBuf[128];
  public:
    Hayes();
    bool setup(Stream *stream);
    void process();  //process incoming data
    void processResponse(const char *input);
    
    HayesDebug onDebug;
    HayesDelegate onStateChanged;
    
    //properties
    bool getInCall();
    bool getSignalLevel();
    const char *getProviderName();
    
    //actions
    bool getCallReady()
    {
      return isCallReady;
    }
    bool sendCommandBlocking(const char *command);
    bool sendCommand(const char *command, HayesResponseHandler *responseHandler);
    bool dialNumber(const char *number, HayesDelegate *completion);
    bool hangUp(HayesDelegate *completion);
    bool pickUp(HayesDelegate *completion);
};
