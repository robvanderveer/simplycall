

int pin_power = 8;
int pin_pwrdetect = 10;
int pin_ringer = A0;

unsigned long lastMillis = 0;
unsigned long lastPressed;
bool onOff;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(pin_ringer, OUTPUT); 
  
  //TODO: Delay to make sure the owner really wants to boot the device.
  
  //turn the power on startup.
  pinMode(pin_power, OUTPUT);
  digitalWrite(pin_power, true);
  
  //listen to switch on pin 3.
  pinMode(pin_pwrdetect, INPUT_PULLUP);
  
  tone(pin_ringer, 4000, 50);
  delay(200);
}

// the loop routine runs over and over again forever:
void loop() 
{
  if(millis() - lastMillis > 500)
  {
    tone(pin_ringer, 2000, 50);
    lastMillis = millis();
  }
 
 //power switch pressed?
  if(digitalRead(pin_pwrdetect) == false)
  {
    //digitalWrite(pin_power, false); 
    
     noTone(pin_ringer);
     tone(pin_ringer, 3000, 10);
     
      if(millis() - lastPressed > 1000)
      { 
          tone(pin_ringer, 440, 50);
          delay(200);
           tone(pin_ringer, 440, 50);
          delay(200);
           tone(pin_ringer, 440, 50);
          delay(200);
           tone(pin_ringer, 440, 50);
          delay(200);
          
         //power off!
         digitalWrite(pin_power, false); 
         
         tone(pin_ringer, 1000, 50);
         digitalWrite(pin_power, false); 
         
         delay(5000);
         
         while(true);//end
      }
   } 
   else
   {
     lastPressed = millis();
   }
   
   delay(50);
}
