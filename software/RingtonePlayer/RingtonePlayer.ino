
#include "pitches.h"

int const ringtone_Calling[] = 
{
  NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
   NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
    NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
    NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
  0, 4,
    NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
   NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
    NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
    NOTE_C5, 32,
  NOTE_C6, 32,
  NOTE_C7, 32,
  0, 1,
  0, 1,
  0, 1,
  -1  //end.
}; 

uint8_t ringtonePos = 0;
const int *ringToneCurrent;

const int PIN_SPEAKER = A0;

void ringtoneStart(const int *melody)
{
  ringtonePos = 0;
  ringToneCurrent = melody;
  //sound.schedule(0, &ringtonePlayer);
}

int ringtonePlayer()
{
  switch(ringToneCurrent[ringtonePos])
  {
  case -1:
    ringtonePos = 0;
    return 1;
  case -2:
    //todo: no loop.
    ringtonePos = 0;
    return 1;
  default:
    int pitch = ringToneCurrent[ringtonePos];
    int duration = 1000 / ringToneCurrent[ringtonePos+1];
    if(pitch > 0)
    {
      tone(PIN_SPEAKER, pitch, duration);
    }
    else
    {
      noTone(PIN_SPEAKER);
    }

    ringtonePos+=2;
    return duration * 1.2;  
  }
}

void setup() 
{
  ringtoneStart(ringtone_Calling);
}

void loop()
{
  // no need to repeat the melody.
  int duration = ringtonePlayer();
  digitalWrite(11, true);
  delay(duration);
}

