
// two pins give me 4 baudrate settings.
const uint8_t BIT0_PIN = 10;
const uint8_t BIT1_PIN = 9;
const uint8_t DEBUG_PIN = 8;    //outputs settings info on boot.

void setup()
{
  pinMode(BIT0_PIN, INPUT_PULLUP);
  pinMode(BIT1_PIN, INPUT_PULLUP);
  pinMode(DEBUG_PIN, INPUT_PULLUP);
  
  bool debug = !digitalRead(DEBUG_PIN);
  
  bool bit0 = !digitalRead(BIT0_PIN);
  bool bit1 = !digitalRead(BIT1_PIN);
  
  uint8_t baudSelect = bit0 | (bit1 << 1);
  unsigned int baudRate;
  
  switch(baudSelect)
  {
     case 0:
       baudRate = 9600;
       break;
     case 1:
       baudRate = 19200;
       break; 
     case 2:
       baudRate = 38400;
       break; 
     case 3:
       baudRate = 57600;
       break; 
  }
  
  Serial.begin(baudRate);

  if(debug)
  {
    Serial.write(bit0?'X':'0');
    Serial.write(bit1?'X':'0');
    delay(500);
  }
}

void loop()
{
  while(Serial.available() > 0)
  {
    char c = Serial.read();
    Serial.write(c); 
  }
  
  delay(1);
}

